"""Leetcode Problem 9
https://leetcode.com/problems/palindrome-number/
Runtime: 52 ms, faster than 97.35% of Python3 online submissions for Palindrome Number.
Memory Usage: 14 MB, less than 18.69% of Python3 online submissions for Palindrome Number.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()

        self.assertEqual(solution.isPalindrome(121), True, "121 should be a palindrome")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()

        self.assertEqual(
            solution.isPalindrome(-121), False, "-121 should not be a palindrome"
        )

    def testExample3(self):
        """Test example 3 provided by Leetcode."""
        solution = Solution()

        self.assertEqual(solution.isPalindrome(10), False, "10 should be a palindrome")


class Solution:
    """Leetcode-provided Solution class."""

    def isPalindrome(self, x: int) -> bool:  # noqa pylint: no-self-use
        """Compares the input to a mirrored version and returns True if both are the same,
        False if not.

        Args:
            x: Integer to check.

        Returns:
            A Boolean describing whether or not the given integer is a palindrome.
        """
        return str(x) == str(x)[::-1]


if __name__ == "__main__":
    unittest.main()
