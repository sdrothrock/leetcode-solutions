"""Leetcode Problem 383
https://leetcode.com/problems/ransom-note/
Runtime: 84 ms, faster than 50.04% of Python3 online submissions for Ransom Note.
Memory Usage: 14.4 MB, less than 10.61% of Python3 online submissions for Ransom Note.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        ransomNote = "a"
        magazine = "b"

        solution = Solution()

        self.assertEqual(
            solution.canConstruct(ransomNote, magazine),
            False,
            f"Cannot construct {ransomNote} from {magazine}",
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        ransomNote = "aa"
        magazine = "ab"

        solution = Solution()

        self.assertEqual(
            solution.canConstruct(ransomNote, magazine),
            False,
            f"Cannot construct {ransomNote} from {magazine}",
        )

    def testExample3(self):
        """Test example 3 provided by Leetcode."""
        ransomNote = "aa"
        magazine = "aab"

        solution = Solution()

        self.assertEqual(
            solution.canConstruct(ransomNote, magazine),
            True,
            f"Can construct {ransomNote} from {magazine}",
        )


class Solution:
    """Leetcode-provided Solution class."""

    def canConstruct(  # noqa pylint: no-self-use
        self, ransomNote: str, magazine: str
    ) -> bool:
        """Determines whether it's possible to construct the ransom note with a given
        magazine.

        Args:
            ransomNote: String containing the note to construct.
            magazine: String containing the letters available from a magazine.

        Returns:
            A boolean describing if it's possible to construct the ransom note.
        """
        # Not a fan of camel case here, but since leetcode used it for variables
        # I'll stick with the existing convention
        magazineCharList = [char for char in magazine]  # noqa: unnecessary-comprehension

        for char in ransomNote:
            try:
                magazineIndex = magazineCharList.index(char)
            except ValueError:
                return False

            magazineCharList.pop(magazineIndex)

        return True


if __name__ == "__main__":
    unittest.main()
