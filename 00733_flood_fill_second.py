"""Leetcode Problem 733
https://leetcode.com/problems/flood-fill/
Runtime: 112 ms, faster than 13.70% of Python3 online submissions for Flood Fill.
Memory Usage: 13.9 MB, less than 91.19% of Python3 online submissions for Flood Fill.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        print("===== Example 1 =====")
        image = [
            [1, 1, 1],
            [1, 1, 0],
            [1, 0, 1],
        ]

        expectedImage = [
            [2, 2, 2],
            [2, 2, 0],
            [2, 0, 1],
        ]

        solution = Solution()
        filledImage = solution.floodFill(image, 1, 1, 2)

        self.assertEqual(
            filledImage, expectedImage, f"{filledImage}\nshould be\n{expectedImage}"
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        print("===== Example 2 =====")
        image = [
            [0, 0, 0],
            [0, 0, 0],
        ]

        expectedImage = [
            [2, 2, 2],
            [2, 2, 2],
        ]

        solution = Solution()
        filledImage = solution.floodFill(image, 0, 0, 2)

        self.assertEqual(
            filledImage, expectedImage, f"{filledImage}\nshould be\n{expectedImage}"
        )

    def testFailedCase1(self):
        """Test case 1 provided by Leetcode after submission failure."""
        print("===== Failed Case 1 =====")
        image = [
            [0, 0, 0],
            [0, 1, 0],
        ]

        expectedImage = [
            [0, 0, 0],
            [0, 2, 0],
        ]

        solution = Solution()
        filledImage = solution.floodFill(image, 1, 1, 2)

        self.assertEqual(
            filledImage, expectedImage, f"{filledImage}\nshould be\n{expectedImage}"
        )

    def testfloodFillInit(self):
        """Test Solution initialization."""
        print("===== Init =====")
        image = [
            [1, 5, 9],
            [2, 6, 10],
            [3, 7, 11],
            [4, 8, 12],
        ]

        solution = Solution()
        _ = solution.floodFill(image, 1, 1, 1)

        self.assertEqual(
            solution.image,
            image,
            "Solution().image should match TestSolution.image",
        )
        self.assertEqual(solution.rowCount, 4, "Solution().rowCount should be 4")
        self.assertEqual(solution.colCount, 3, "Solution().colCount should be 3")


class Solution:
    """Leetcode-provided solution class."""

    image = []
    rowCount = 0
    colCount = 0
    searchedList = []

    def findAndUpdateAdjacentMatches(
        self, startCoords: tuple, toMatch: int, newColor: int
    ):
        """Flood fill an image starting from the given coordinates and recursing in four
        directions until all possible matches have been converted to the new color.

        Args:
            startCoords: Tuple containing the row and column to start the fill at.
            toMatch: Integer containing the value to match for filling.
            newColor: Integer to replace matches with.
        """
        startRow, startCol = startCoords

        print(f"In findAndUpdateAdjacentMatches {self.searchedList}")
        print(f"Checking image {self.image} {self.rowCount} rows, {self.colCount} cols")

        if (
            startRow < 0  # noqa pylint: too-many-boolean-expressions
            or startCol < 0
            or startRow >= self.rowCount
            or startCol >= self.colCount
            or startCoords in self.searchedList
            or self.image[startRow][startCol] != toMatch
        ):
            return

        print(
            f"Examining ({startRow}, {startCol}), {self.image[startRow][startCol]} ?= {toMatch}"
        )

        self.searchedList.append(startCoords)
        self.image[startRow][startCol] = newColor

        self.findAndUpdateAdjacentMatches((startRow + 1, startCol), toMatch, newColor)
        self.findAndUpdateAdjacentMatches((startRow - 1, startCol), toMatch, newColor)
        self.findAndUpdateAdjacentMatches((startRow, startCol + 1), toMatch, newColor)
        self.findAndUpdateAdjacentMatches((startRow, startCol - 1), toMatch, newColor)

    def floodFill(
        self, image: list[list[int]], sr: int, sc: int, newColor: int
    ) -> list[list[int]]:
        """Perform a four-way flood fill on an image starting from the given row and
        column, replacing the value from those coordinates.

        Args:
            image: List of lists representing an image, with each cell containing an
                   integer.
            sr: Integer indicating the row to start at.
            sc: Integer indicating the column to start at.
            newColor: Integer to flood fill with.

        Returns:
            A list of lists that has been flood filled with the new color.
        """
        self.image = image
        self.rowCount = len(image)
        self.colCount = len(image[0])
        self.searchedList = []

        print("Running floodFill")

        self.findAndUpdateAdjacentMatches((sr, sc), image[sr][sc], newColor)

        return self.image


if __name__ == "__main__":
    unittest.main()
