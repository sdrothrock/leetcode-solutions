"""Leetcode Problem 1
https://leetcode.com/problems/two-sum/
Runtime: 3206 ms, faster than 33.03% of Python3 online submissions for Two Sum.
Memory Usage: 14.4 MB, less than 45.75% of Python3 online submissions for Two Sum.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()

        nums = [2, 7, 11, 15]
        target = 9
        indexes = solution.twoSum(nums, target)
        indexes.sort()

        self.assertEqual(
            indexes, [0, 1], "nums[0] + nums[1] = 9, so the indexes are [0, 1]"
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()

        nums = [3, 2, 4]
        target = 6
        indexes = solution.twoSum(nums, target)
        indexes.sort()

        self.assertEqual(
            indexes, [1, 2], "nums[1] + nums[2] = 6, so the indexes are [1, 2]"
        )

    def testExample3(self):
        """Test example 3 provided by Leetcode."""
        solution = Solution()

        nums = [3, 3]
        target = 6
        indexes = solution.twoSum(nums, target)
        indexes.sort()

        self.assertEqual(
            indexes, [0, 1], "nums[0] + nums[1] = 6, so the indexes are [0, 1]"
        )


class Solution:
    """Leetcode-provided Solution class."""

    def twoSum(self, nums: list, target: int) -> list:  # noqa pylint: no-self-use
        """Returns the indices of two numbers that, when added, are equal to a target
        number.

        Args:
            nums: A list of numbers to search.
            target: The target number that two numbers from nums must add up to match.

        Returns:
            A list containing two indices that point to a pair of numbers that,
            when added, match the target number.
        """
        if len(nums) == 2:
            return [0, 1]

        for i in range(0, len(nums)):  # noqa pylint: consider-using-enumerate
            # Start at i + 1 since we've already tried all possible combinations
            # up to there + 1 because we can only use each element once,
            # so skip the i-th element
            for j in range(i + 1, len(nums)):
                if nums[i] + nums[j] == target:
                    return [i, j]

        return [None, None]


if __name__ == "__main__":
    unittest.main()
