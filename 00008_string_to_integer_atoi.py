"""Leetcode Problem 8
https://leetcode.com/problems/string-to-integer-atoi/
Runtime: 24 ms, faster than 99.89% of Python3 online submissions for String to Integer (atoi).
Memory Usage: 13.9 MB, less than 33.43% of Python3 online submissions for String to Integer (atoi).
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()

        self.assertEqual(solution.myAtoi("42"), 42, '"42" should be 42')

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()

        self.assertEqual(solution.myAtoi("   -42"), -42, '"   -42" should be -42')

    def testExample3(self):
        """Test example 3 provided by Leetcode."""
        solution = Solution()

        solution = Solution()

        self.assertEqual(
            solution.myAtoi("4193 with words"), 4193, '"4193 with words" should be 4193'
        )

    def testFailedCase1(self):
        """Test case 1 provided by Leetcode after submission failure."""
        solution = Solution()

        self.assertEqual(
            solution.myAtoi("words and 987"),
            0,
            '"words and 987" has no valid numerals at the beginning and should be 0',
        )

    def testFailedCase2(self):
        """Test case 2 provided by Leetcode after submission failure."""
        solution = Solution()

        self.assertEqual(
            solution.myAtoi(""),
            0,
            '"" has no valid numerals and should be 0',
        )


class Solution:
    """Leetcode-provided Solution class."""

    lowerLimit = -(2**31)
    upperLimit = 2**31 - 1

    def getBoundedInt(self, sign: int, inStr: str) -> int:
        """Converts input and returns a signed int between the lower and upper ranges.

        Args:
            sign: Integer with a value of 1 or -1 to be used as the sign for the final
                  output.
            inStr: String to convert to an integer.

        Returns:
            A signed 32-bit integer bounded by lower and upper limits.
        """
        if not inStr.isdigit():
            return 0

        outInt = sign * int(inStr)

        if outInt < self.lowerLimit:
            return self.lowerLimit

        if outInt > self.upperLimit:
            return self.upperLimit

        return outInt

    def myAtoi(self, s: str) -> int:
        """Prepares a string for conversion into a signed 32-bit integer and returns the
        result.

        Args:
            s: String to be converted to a signed 32-bit integer.

        Returns:
            A signed 32-bit integer bounded by upper and lower limits.
        """
        strippedStr = s.strip()

        if len(strippedStr) == 0:
            return 0

        sign = 1

        if strippedStr[0] in ["+", "-"]:
            if strippedStr[0] == "-":
                sign = -1

            strippedStr = strippedStr[1:]

        for i in range(1, len(strippedStr) + 1):
            if not strippedStr[:i].isdigit():
                return self.getBoundedInt(sign, strippedStr[: (i - 1)])

        return self.getBoundedInt(sign, strippedStr)


if __name__ == "__main__":
    unittest.main()
