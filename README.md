# Scott Rothrock's Leetcode Solutions

Using Python 3.10 with Black, flake8

## General "Rules"
- Don't look at other solutions/algorithms before attempting
- Look at other solutions/algorithms **after** attempting
- OK to look up Python functionality

## Highlights
- [Add Two Numbers](00002_add_two_numbers.py): Faster and more efficient than many entries because I converted the provided `ListNode`s to `int` and performed normal math, then converted the result back
