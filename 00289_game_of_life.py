"""Leetcode Problem 289
https://leetcode.com/problems/game-of-life/
Runtime: 36 ms, faster than 85.68% of Python3 online submissions for Game of Life.
Memory Usage: 14 MB, less than 51.25% of Python3 online submissions for Game of Life.
"""

import copy
import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()  # noqa: F841

        board = [
            [0, 1, 0],
            [0, 0, 1],
            [1, 1, 1],
            [0, 0, 0],
        ]

        expected = [
            [0, 0, 0],
            [1, 0, 1],
            [0, 1, 1],
            [0, 1, 0],
        ]

        solution.gameOfLife(board)
        self.assertEqual(board, expected, "Boards should match")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()  # noqa: F841

        board = [
            [1, 1],
            [1, 0],
        ]

        expected = [
            [1, 1],
            [1, 1],
        ]

        solution.gameOfLife(board)
        self.assertEqual(board, expected, "Boards should match")


class Solution:
    """Leetcode-provided Solution class."""

    boardBuffer = []
    width = 0
    height = 0

    def getNeighborCounts(self, row: int, col: int) -> dict:
        """Counts the values of the horizontal, vertical, and diagonal neighbors.

        args:
            row: Integer of the row to start looking for neighbors from.
            col: Integer of the col to start looking for neighbors from.

        Returns:
            A dict containing the total number of 0s and 1s in the surrounding neighbors.
        """
        countDict = {
            0: 0,  # Dead count
            1: 0,  # Alive count
        }

        for neighboringRow in [-1, 0, 1]:
            for neighboringCol in [-1, 0, 1]:
                if neighboringRow == 0 and neighboringCol == 0:
                    continue

                checkRow = row + neighboringRow
                checkCol = col + neighboringCol

                if 0 <= checkRow < self.height and 0 <= checkCol < self.width:
                    countDict[self.boardBuffer[checkRow][checkCol]] += 1

        return countDict

    def gameOfLife(self, board: list[list[int]]) -> None:
        """Runs Conway's Game of Life for on tick, modifying the board in place.

        Rules:
            1. Any live cell with fewer than two live neighbors dies as if caused by
               under-population.
            2. Any live cell with two or three live neighbors lives on to the next
               generation.
            3. Any live cell with more than three live neighbors dies, as if by
               over-population.
            4. Any dead cell with exactly three live neighbors becomes a live cell, as if
               by reproduction.

        args:
            board: A list of lists, each containing the number 1 (live) or 0 (dead).

        Returns:
            Nothing.
        """
        self.boardBuffer = copy.deepcopy(board)

        self.width = len(self.boardBuffer[0])
        self.height = len(self.boardBuffer)

        for rowCoord, row in enumerate(self.boardBuffer):
            for colCoord, _ in enumerate(row):
                countDict = self.getNeighborCounts(rowCoord, colCoord)

                if self.boardBuffer[rowCoord][colCoord] == 0:
                    if countDict[1] == 3:
                        board[rowCoord][colCoord] = 1
                    else:
                        # Not necessary, but included for clarity
                        board[rowCoord][colCoord] = 0
                else:
                    if countDict[1] < 2 or countDict[1] > 3:
                        # Death from under-population or over-population
                        board[rowCoord][colCoord] = 0
                    else:
                        # Not necessary, but included for clarity
                        board[rowCoord][colCoord] = 1


if __name__ == "__main__":
    unittest.main()
