"""Leetcode Problem 1046
https://leetcode.com/problems/last-stone-weight/
Runtime: 24 ms, faster than 99.21% of Python3 online submissions for Last Stone Weight.
Memory Usage: 13.9 MB, less than 68.03% of Python3 online submissions for Last Stone Weight.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()
        stones = [2, 7, 4, 1, 8, 1]

        self.assertEqual(
            solution.lastStoneWeight(stones), 1, "The last remaining stone should weigh 1"
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()
        stones = [1]

        self.assertEqual(
            solution.lastStoneWeight(stones),
            1,
            "The only stone is the last remaining stone and weighs 1",
        )


class Solution:
    """Leetcode-provided Solution class."""

    def lastStoneWeight(self, stones: list[int]) -> int:  # noqa pylint: no-self-use
        """Given a list of numbers, compare the two largest numbers. If they are equal,
        both numbers are removed from the list; if they are not equal, then the smaller
        number is subtracted from the larger number, then removed from the list.

        This continues until there is one or zero numbers left in the list.

        args:
            stones: List of integers to be compared against each other.

        returns:
            Integer value of the last remaining number, or 0 if there are no numbers.
        """
        stones.sort(reverse=True)

        while len(stones) > 1:
            if stones[0] == stones[1]:
                stones.pop(0)
                stones.pop(0)
            else:
                stones[0] -= stones[1]
                stones.pop(1)

                stones.sort(reverse=True)

        if len(stones) == 1:
            return stones[0]

        return 0


if __name__ == "__main__":
    unittest.main()
