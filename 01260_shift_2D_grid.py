"""Leetcode Problem 1260
https://leetcode.com/problems/shift-2d-grid/
Runtime: 178 ms, faster than 81.89% of Python3 online submissions for Shift 2D Grid.
Memory Usage: 14.3 MB, less than 35.10% of Python3 online submissions for Shift 2D Grid.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        print("===TEST 1===")
        solution = Solution()

        grid = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        k = 1

        result = solution.shiftGrid(grid, k)
        expected = [[9, 1, 2], [3, 4, 5], [6, 7, 8]]

        self.assertEqual(result, expected, f"{result} should match {expected}")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        print("===TEST 2===")
        solution = Solution()

        grid = [[3, 8, 1, 9], [19, 7, 2, 5], [4, 6, 11, 10], [12, 0, 21, 13]]
        k = 4

        result = solution.shiftGrid(grid, k)
        expected = [[12, 0, 21, 13], [3, 8, 1, 9], [19, 7, 2, 5], [4, 6, 11, 10]]

        self.assertEqual(result, expected, f"{result} should match {expected}")

    def testFailedCase1(self):
        """Test case 1 provided by Leetcode after submission failure."""
        print("===FAILED CASE 1===")
        solution = Solution()

        grid = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
        k = 9

        result = solution.shiftGrid(grid, k)
        expected = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

        self.assertEqual(result, expected, f"{result} should match {expected}")


class Solution:
    """Leetcode-provided Solution class."""

    def shiftGrid(  # noqa pylint: no-self-use
        self, grid: list[list[int]], k: int
    ) -> list[list[int]]:
        """Shifts every item in the provided grid by a specified number of steps. If an
        item goes off the edge of the row, it goes to the beginning of the next row. If
        an item goes off the edge of the bottom of the grid, it goes to the beginning of
        the topmost row.

        args:
            grid: A list of lists of integers.
            k: Integer specifying the number of steps to shift items.

        returns:
            A list of lists of integers representing the grid shifted by the specified
            number of steps.
        """

        gridHeight = len(grid)
        gridWidth = len(grid[0])

        newGrid = []

        for row in grid:
            newRow = [None] * gridWidth
            newGrid.append(newRow)

        for rowPos, row in enumerate(grid):
            for colPos, item in enumerate(row):
                newColPos = colPos + k
                newRowPos = rowPos

                if newColPos >= gridWidth:
                    newRowMod, newColPos = divmod(newColPos, gridWidth)
                    newRowPos += newRowMod

                    if newRowPos >= gridHeight:
                        newRowPos = newRowPos % gridHeight

                newGrid[newRowPos][newColPos] = item

        return newGrid


if __name__ == "__main__":
    unittest.main()
