"""Leetcode Problem 347
https://leetcode.com/problems/top-k-frequent-elements/
Runtime: 1744 ms, faster than 5.03% of Python3 online submissions for Top K Frequent Elements.
Memory Usage: 18.6 MB, less than 68.16% of Python3 online submissions for Top K Frequent Elements.
"""

import operator
import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-2."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()
        nums = [1, 1, 1, 2, 2, 3]
        k = 2
        expected = [1, 2]

        self.assertEqual(
            solution.topKFrequent(nums, k), expected, f"Result should be {expected}"
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()
        nums = [1]
        k = 1
        expected = [1]

        self.assertEqual(
            solution.topKFrequent(nums, k), expected, f"Result should be {expected}"
        )


class Solution:
    """Leetcode-provided Solution class."""

    def topKFrequent(  # noqa pylint: no-self-use
        self, nums: list[int], k: int
    ) -> list[int]:
        """Returns a list containing the top K most frequent numbers.

        Args:
            nums: A list of numbers to search.
            k: the number of most frequent numbers to return.

        Returns:
            A list containing the top K most frequent numbers provided.
        """
        countDict = {}

        for key in nums:
            if key not in countDict:
                countDict[key] = nums.count(key)

        mostFrequentList = []

        while len(countDict) > 0 and len(mostFrequentList) < k:
            mostFrequentKey = max(countDict.items(), key=operator.itemgetter(1))[0]
            mostFrequentList.append(mostFrequentKey)
            countDict.pop(mostFrequentKey)

        # Sort for testability
        mostFrequentList.sort()

        return mostFrequentList


if __name__ == "__main__":
    unittest.main()
