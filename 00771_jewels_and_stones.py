"""Leetcode Problem 771
https://leetcode.com/problems/jewels-and-stones/
Runtime: 31 ms, faster than 89.70% of Python3 online submissions for Jewels and Stones.
Memory Usage: 13.8 MB, less than 96.97% of Python3 online submissions for Jewels and Stones.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()
        jewels = "aA"
        stones = "aAAbbbb"

        result = solution.numJewelsInStones(jewels, stones)
        expected = 3

        self.assertEqual(result, expected, f"There should be {expected} jewels.")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()
        jewels = "z"
        stones = "ZZ"

        result = solution.numJewelsInStones(jewels, stones)
        expected = 0

        self.assertEqual(result, expected, f"There should be {expected} jewels.")


class Solution:
    """Leetcode-provided Solution class."""

    def numJewelsInStones(self, jewels: str, stones: str) -> int:  # noqa: no-self-use
        """Returns the total number of jewels among the stones.

        args:
            jewels: String consisting of English letters representing jewels.
            stones: String consisting of English letters representing jewels and stones.

        Returns:
            Integer count of all jewels among the stones.
        """

        count = 0

        for jewel in jewels:
            count += stones.count(jewel)

        return count


if __name__ == "__main__":
    unittest.main()
