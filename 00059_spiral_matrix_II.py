"""Leetcode Problem 59
https://leetcode.com/problems/spiral-matrix-ii/
Runtime: 31 ms, faster than 92.38% of Python3 online submissions for Spiral Matrix II.
Memory Usage: 13.9 MB, less than 85.84% of Python3 online submissions for Spiral Matrix II.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()  # noqa: F841

        n = 3
        expected = [[1, 2, 3], [8, 9, 4], [7, 6, 5]]
        result = solution.generateMatrix(n)

        self.assertEqual(result, expected, "Matrices should match")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()  # noqa: F841

        n = 1
        expected = [[1]]
        result = solution.generateMatrix(n)

        self.assertEqual(result, expected, "Matrices should match")


class Solution:
    """Leetcode-provided Solution class."""

    def generateMatrix(self, n: int) -> list[list[int]]:  # noqa pylint: no-self-use
        """Create a square 2D array with dimensions matching the input. Fill the matrix
        cell by cell, starting in the upper left and moving left, down, up, right, and
        repeating until all cells are filled.

        The numbers should start at 1 and rise by 1 until all cells are filled.

        args:
            n: Integer specifying the dimensions of the output matrix.

        Returns:
            List of lists of integers filled in the specified manner.
        """
        RIGHT = 0
        DOWN = 1
        LEFT = 2
        UP = 3

        move = [
            (0, 1),  # right
            (1, 0),  # down
            (0, -1),  # left
            (-1, 0),  # up
        ]

        boundaries = [
            n - 1,  # right
            n - 1,  # bottom
            0,  # left
            1,  # top, set at 1 because we always start in the upper-left corner
        ]

        finalMatrix = []

        for _ in range(n):
            finalMatrix.append([None] * n)

        currMove = 0
        currRow = 0
        currCol = 0

        for i in range(n * n):
            finalMatrix[currRow][currCol] = i + 1

            currRow += move[currMove][0]
            currCol += move[currMove][1]

            if currMove == RIGHT and currCol == boundaries[RIGHT]:
                currMove = DOWN
                boundaries[RIGHT] -= 1
            elif currMove == DOWN and currRow == boundaries[DOWN]:
                currMove = LEFT
                boundaries[DOWN] -= 1
            elif currMove == LEFT and currCol == boundaries[LEFT]:
                currMove = UP
                boundaries[LEFT] += 1
            elif currMove == UP and currRow == boundaries[UP]:
                currMove = RIGHT
                boundaries[UP] += 1

        return finalMatrix


if __name__ == "__main__":
    unittest.main()
