"""Leetcode Problem 876
https://leetcode.com/problems/middle-of-the-linked-list/
Runtime: 28 ms, faster than 95.43% of Python3 online submissions for Middle of the Linked List.
Memory Usage: 13.9 MB, less than 12.57% of Python3 online submissions for Middle of the Linked List.
"""

import unittest
from math import floor


# Definition for singly-linked list.
class ListNode:
    """A singly-linked list following the Leetcode implementation."""

    def __init__(self, val=0, next=None):  # noqa pylint: redefined-builtin
        self.val = val
        self.next = next  # noqa pylint: redefined-builtin


def convertListNodeToString(head: ListNode) -> str:
    """Convenience method for converting a ListNode to a string for testing purposes.

    Args:
        head: ListNode to be converted to a string.

    Returns:
        String representation of the ListNode.
    """
    curr_node = head
    output_str = ""

    while curr_node:
        output_str += str(curr_node.val)
        curr_node = curr_node.next

    return output_str


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()

        head = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5)))))
        output = solution.middleNode(head)

        self.assertEqual(
            convertListNodeToString(output),
            "345",
            "Should be ListNode([3, 4, 5])",
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()

        head = ListNode(
            1, ListNode(2, ListNode(3, ListNode(4, ListNode(5, ListNode(6)))))
        )
        output = solution.middleNode(head)

        self.assertEqual(
            convertListNodeToString(output),
            "456",
            "Should be ListNode([4, 5, 6])",
        )


class Solution:
    """Leetcode-provided Solution class."""

    def middleNode(self, head: ListNode) -> ListNode:  # noqa pylint: no-self-use
        """Returns the value of the middle node of the ListNode.

        args:
            head: ListNode to search.

        returns:
            ListNode matching the middle node of the Listnode.
        """
        listNode_list = []

        curr_node = head

        while curr_node:
            listNode_list.append(curr_node)
            curr_node = curr_node.next

        list_len = len(listNode_list)
        middle_index = floor(list_len / 2)

        return listNode_list[middle_index]


if __name__ == "__main__":
    unittest.main()
