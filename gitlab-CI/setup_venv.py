"""Gitlab CI will run this script to create the static analysis venv when neccessary."""

import os.path
import venv

venv_path = "./static-analysis-venv"

if os.path.isdir(venv_path):
    print(f"venv already exists at {venv_path}")
    print("Skipping venv creation")
else:
    print(f"No venv found at {venv_path}")
    print(f"Creating venv at {venv_path}")
    venv.create(venv_path, with_pip=True)
    print(f"Created venv at {venv_path}")
