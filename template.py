"""Leetcode Problem
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()  # noqa: F841
        result = solution.solution()
        self.assertEqual(result, True, "")


class Solution:
    """Leetcode-provided Solution class."""

    def solution(self):  # noqa pylint: no-self-use
        """Solution template."""
        return True


if __name__ == "__main__":
    unittest.main()
