"""Leetcode Problem 13 (second approach)
https://leetcode.com/problems/roman-to-integer/
Runtime: 73 ms, faster than 42.76% of Python3 online submissions for Roman to Integer.
Memory Usage: 14 MB, less than N/A of Python3 online submissions for Roman to Integer.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        s = "III"

        solution = Solution()
        int_from_roman = solution.romanToInt(s)

        self.assertEqual(int_from_roman, 3, "III should be 3")

    def testExample2(self):

        """Test example 2 provided by Leetcode."""
        s = "LVIII"

        solution = Solution()
        int_from_roman = solution.romanToInt(s)

        self.assertEqual(int_from_roman, 58, "LVIII should be 58")

    def testExample3(self):

        """Test example 3 provided by Leetcode."""
        s = "MCMXCIV"

        solution = Solution()
        int_from_roman = solution.romanToInt(s)

        self.assertEqual(int_from_roman, 1994, "MCMXCIV should be 1994")


class Solution:
    """Leetcode-provided Solution class."""

    __valid_numerals = ("I", "V", "X", "L", "C", "D", "M")
    __numeral_conversion = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000,
    }

    def __containsOnlyRomanNumerals(self, s: str) -> bool:
        """Checks the string and returns True if it contains only valid Roman numerals,
        False if it contains anything else.

        Args:
            s: String to be checked.

        Returns:
            A boolean describing whether the input contains non-Roman numeral characters.
        """
        return any(  # noqa: use-a-generator
            [numeral in s for numeral in self.__valid_numerals]
        )

    def romanToInt(self, s: str) -> int:
        """Converts the input string of Roman numerals to an integer..

        Args:
            s: String to be converted to an integer.

        Returns:
            An integer converted from the input string of Roman numerals.
        """

        if len(s) < 1 or len(s) > 15:
            raise Exception("Roman numeral must be between 1 and 15 letters long.")

        if self.__containsOnlyRomanNumerals is False:
            raise Exception(
                f"Roman numeral must contain only one of {self.__valid_numerals}"
            )

        result = 0
        last_val = 0

        for numeral in reversed(s):
            curr_val = self.__numeral_conversion[numeral]

            if curr_val < last_val:
                result -= curr_val
            else:
                result += curr_val

            last_val = curr_val

        if result < 1 or result > 3999:
            raise Exception(
                "Assignment guarantees that the roman numeral is between 1 and 3999"
            )

        return result


if __name__ == "__main__":
    unittest.main()
