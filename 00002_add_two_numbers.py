"""Leetcode Problem 2
https://leetcode.com/problems/add-two-numbers/
Runtime: 64 ms, faster than 97.68% of Python3 online submissions for Add Two Numbers.
Memory Usage: 13.9 MB, less than 89.28% of Python3 online submissions for Add Two Numbers.
"""

import unittest


class ListNode:
    """A singly-linked list following the Leetcode implementation."""

    def __init__(self, val=0, next=None):  # noqa pylint: redefined-builtin
        self.val = val
        self.next = next


def getLtoRInt(self):
    """Fetches and concatenates values of this node and all nodes after this node. The
    concatenated value is then reversed and returned as an integer.

    In this assignment, each node is assumed to contain a value from 0-9. These numbers,
    when concatenated as a string, are an integer written from right to left.

    Thus, 1 -> 0 -> 7 -> 1 represents the integer 1701.

    Returns:
        The integer represented by the singly-linked list.
    """
    next_node = self
    val_list = []

    while next_node is not None:
        val_list.append(str(next_node.val))
        next_node = next_node.next

    return int("".join(val_list[::-1]))


def append(self, in_int: int):
    """Appends a new node at the end of the singly-linked list."""
    new_node = ListNode(in_int)

    curr_node = self

    while curr_node.next is not None:
        curr_node = curr_node.next

    curr_node.next = new_node


# Add the functions to the pre-existing Leetcode ListNode() implementation.
ListNode.getLtoRInt = getLtoRInt
ListNode.append = append


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3.

    N.B.: The tests initialize the ListNodes with chained calls rather than the
          append() convenience method so that they are more strictly in line with the
          examples as written by Leetcode.
    """

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        l1 = ListNode(2, ListNode(4, ListNode(3)))
        l2 = ListNode(5, ListNode(6, ListNode(4)))

        solution = Solution()
        l3 = solution.addTwoNumbers(l1, l2)

        # ListNode as provided by leetcode doesn't include comparisons
        # Use ListNode.getLtoRInt(), which I made for this problem since it's
        # simpler to do an integer comparison instead
        self.assertEqual(l3.getLtoRInt(), 807, "342 + 465 = 807")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        l1 = ListNode(0)
        l2 = ListNode(0)

        solution = Solution()
        l3 = solution.addTwoNumbers(l1, l2)

        # ListNode as provided by leetcode doesn't include comparisons
        # Use ListNode.getLtoRInt(), which I made for this problem since it's
        # simpler to do an integer comparison instead
        self.assertEqual(l3.getLtoRInt(), 0, "0 + 0 = 0")

    def testExample3(self):
        """Test example 3 provided by Leetcode."""

        l1 = ListNode(
            9,
            ListNode(9, ListNode(9, ListNode(9, ListNode(9, ListNode(9, ListNode(9)))))),
        )
        l2 = ListNode(9, ListNode(9, ListNode(9, ListNode(9))))

        solution = Solution()
        l3 = solution.addTwoNumbers(l1, l2)

        # ListNode as provided by leetcode doesn't include comparisons
        # Use ListNode.getLtoRInt(), which I made for this problem since it's
        # simpler to do an integer comparison instead
        self.assertEqual(l3.getLtoRInt(), 10009998, "9999999 + 9999 = 10009998")


class Solution:
    """Leetcode-provided Solution class."""

    def addTwoNumbers(  # noqa pylint: no-self-use
        self, l1: ListNode, l2: ListNode
    ) -> ListNode:
        """Adds the contents of two ListNodes and returns the sum.

        Args:
            l1: ListNode describing a number in right-to-left order.
            l2: ListNode describing a number in right-to-left order.

        Returns:
            A ListNode describing the sum of l1 and l1 in right-to-left order.
        """

        l1_int = l1.getLtoRInt()
        l2_int = l2.getLtoRInt()

        reversed_sum_str = str(l1_int + l2_int)[::-1]

        out_listnode = ListNode(reversed_sum_str[0])

        for number in reversed_sum_str[1:]:
            out_listnode.append(number)

        return out_listnode


if __name__ == "__main__":
    unittest.main()
