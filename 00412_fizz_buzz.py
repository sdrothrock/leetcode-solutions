"""Leetcode Problem 412
https://leetcode.com/problems/fizz-buzz

Runtime: 59 ms, faster than 53.70% of Python3 online submissions for Fizz Buzz.
Memory Usage: 15 MB, less than 46.31% of Python3 online submissions for Fizz Buzz.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Tests example 1 provided by Leetcode."""
        solution = Solution()

        n = 3
        expected = ["1", "2", "Fizz"]

        self.assertEqual(solution.fizzBuzz(n), expected, f"{n} should be {expected}")

    def testExample2(self):
        """Tests example 2 provided by Leetcode."""
        solution = Solution()

        n = 5
        expected = ["1", "2", "Fizz", "4", "Buzz"]

        self.assertEqual(solution.fizzBuzz(n), expected, f"{n} should be {expected}")

    def testExample3(self):
        """Tests example 3 provided by Leetcode."""
        solution = Solution()

        n = 15
        expected = [
            "1",
            "2",
            "Fizz",
            "4",
            "Buzz",
            "Fizz",
            "7",
            "8",
            "Fizz",
            "Buzz",
            "11",
            "Fizz",
            "13",
            "14",
            "FizzBuzz",
        ]

        self.assertEqual(solution.fizzBuzz(n), expected, f"{n} should be {expected}")


class Solution:
    """Leetcode-provided Solution class."""

    def fizzBuzz(self, n: int) -> list[str]:  # noqa pylint: no-self-use
        """Returns a list of numbers where every number divisible by 15 is replaced by
        "FizzBuzz", every number divisible by 5 is replaced by "Buzz", every number
        divisible by 3 is replaced by "Fizz", and all other numbers are left untouched.

        Args:
            n: The number of numbers to print.

        Returns:
            A list of strings containing a number, "Fizz", "Buzz", or "FizzBuzz".
        """
        fizzbuzz_list = []

        for i in range(1, n + 1):
            if i % 15 == 0:
                curr_val = "FizzBuzz"
            elif i % 5 == 0:
                curr_val = "Buzz"
            elif i % 3 == 0:
                curr_val = "Fizz"
            else:
                curr_val = str(i)

            fizzbuzz_list.append(curr_val)

        return fizzbuzz_list


if __name__ == "__main__":
    unittest.main()
