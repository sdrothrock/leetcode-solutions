# Based on Gitlab-provided template at
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml

# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/
image: python:3.10

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# Cache pip and virtal env
cache:
  paths:
    - .cache/pip
    - static-analysis-venv/

stages:
  - prepare
  - clean
  - test

prepare-job:
  stage: prepare

  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
      when: always
      allow_failure: false

  script:
    - echo "Starting preparation"
    - python3 --version
    - python3 gitlab-CI/setup_venv.py  # Will only make venv if the venv does not exist
    - source static-analysis-venv/bin/activate
    - echo "Installing static analysis requirements"
    - which python3
    - python3 -m pip freeze
    - python3 -m pip install -r gitlab-CI/static-analysis.txt
    - python3 -m pip freeze
    - echo "Finished installing static analysis requirements"
    - cat pyproject.toml

black-job:
  stage: clean

  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
      when: always
      allow_failure: false

  script:
    - source static-analysis-venv/bin/activate
    - which python3
    - python3 -m pip freeze
    - echo "Running Black on all Python files"
    - black .
    - echo "Finished Black"

isort-job:
  stage: clean

  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
      when: always
      allow_failure: false

  script:
    - source static-analysis-venv/bin/activate
    - which python3
    - python3 -m pip freeze
    - echo "Running isort on all Python files"
    - isort .
    - echo "Finished isort"

pflake8-job:
  stage: clean

  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"'
      when: always
      allow_failure: false

  script:
    - source static-analysis-venv/bin/activate
    - which python3
    - python3 -m pip freeze
    - echo "Running pflake8 on all Python files"
    - pflake8 .
    - echo "Finished pflake8"

unit-test-job:
  stage: test

  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME'
      when: always
      allow_failure: false

  script:
    - source static-analysis-venv/bin/activate
    - which python3
    - python3 -m pip freeze
    - echo "Running unit tests on all Python files"
    - python3 -m unittest -v *.py
