"""Leetcode Problem 682
https://leetcode.com/problems/baseball-game/
Runtime: 39 ms, faster than 93.49% of Python3 online submissions for Baseball Game.
Memory Usage: 14.1 MB, less than 74.69% of Python3 online submissions for Baseball Game.
"""

import unittest


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-2."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()
        ops = ["5", "2", "C", "D", "+"]
        expected = 30

        self.assertEqual(
            solution.calPoints(ops), expected, f"Result should be {expected}"
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()
        ops = ["5", "-2", "4", "C", "D", "9", "+", "+"]
        expected = 27

        self.assertEqual(
            solution.calPoints(ops), expected, f"Result should be {expected}"
        )

    def testExample3(self):
        """Test example 3 provided by Leetcode."""
        solution = Solution()
        ops = ["1"]
        expected = 1

        self.assertEqual(
            solution.calPoints(ops), expected, f"Result should be {expected}"
        )


class Solution:
    """Leetcode-provided Solution class."""

    def calPoints(self, ops: list[str]) -> int:  # noqa pylint: no-self-use
        """Processes the input and returns a final score, following these rules:

        1. Integer: add score to buffer.
        2. "+": Add the sum of the two previous buffered scores to the buffer.
        3. "D": Add 2x the previous buffered score to the buffer.
        4. "C": Remove the last score from the buffer.

        Args:
            ops: A list of operations to perform.

        Returns:
            The sum of all numbers in the buffer.
        """
        buf = []

        for op in ops:
            if "-" in op:
                buf.append(int(op))
            elif op.isdigit():
                buf.append(int(op))
            elif op == "+":
                buf.append(buf[-1] + buf[-2])
            elif op == "D":
                buf.append(buf[-1] * 2)
            elif op == "C":
                del buf[-1]
            else:
                raise Exception(f"Unexpected op code {op}")

        # It's possible to keep a running tally through the operations rather than doing
        # a sum here, but I think that's more difficult to maintain and the performance
        # gains are negligible.
        return sum(buf)


if __name__ == "__main__":
    unittest.main()
