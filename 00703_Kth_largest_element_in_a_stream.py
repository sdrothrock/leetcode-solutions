"""Leetcode Problem 703
https://leetcode.com/problems/kth-largest-element-in-a-stream/
Runtime: 151 ms, faster than 44.79% of Python3 online submissions for Kth Largest Element in a Stream.  # noqa: line-too-long
Memory Usage: 18.3 MB, less than 55.88% of Python3 online submissions for Kth Largest Element in a Stream. # noqa: line-too-long
"""

import unittest
from bisect import insort


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided example 1."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        k = 3
        nums = [4, 5, 8, 2]
        kthLargest = KthLargest(k, nums)

        self.assertEqual(kthLargest.add(3), 4, f"{k}rd largest value should be 4")
        self.assertEqual(kthLargest.add(5), 5, f"{k}rd largest value should be 5")
        self.assertEqual(kthLargest.add(10), 5, f"{k}rd largest value should be 5")
        self.assertEqual(kthLargest.add(9), 8, f"{k}rd largest value should be 8")
        self.assertEqual(kthLargest.add(4), 8, f"{k}rd largest value should be 8")


class KthLargest:
    """Leetcode-provided KthLargest class."""

    k = 0
    stream = []

    def __init__(self, k: int, nums: list[int]):
        """Create a KthLargest object.

        Args:
            k: Integer representing the Kth place.
            nums: List of integers to initialize the stream.
        """

        self.stream = sorted(nums)
        self.k = k

    def add(self, val: int) -> int:
        """Add input to the stream and return the Kth largest value.

        Args:
            val: Integer to add to the stream.

        Returns:
            Integer that is the Kth largest value in the stream.
        """
        insort(self.stream, val)

        return self.stream[-self.k]


# Your KthLargest object will be instantiated and called as such:
# obj = KthLargest(k, nums)
# param_1 = obj.add(val)

if __name__ == "__main__":
    unittest.main()
