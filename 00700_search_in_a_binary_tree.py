"""Leetcode Problem 700
https://leetcode.com/problems/search-in-a-binary-search-tree/
Runtime: 83 ms, faster than 79.14% of Python3 online submissions for Search in a Binary Search Tree.
Memory Usage: 16.4 MB, less than 96.41% of Python3 online submissions for Search in a Binary Search Tree.  # noqa: line-too-long
"""

import unittest


class TreeNode:
    """Definition for a binary tree node from Leetcode"""

    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def insert(self, val):
    """Insert a value into the binary tree. If it is lower than the current value,
    it is inserted to the left side; if it is higher, then it is inserted to the
    right side. If it is equal, it is discarded.

    args:
        val: Integer value to be inserted in the binary tree.

    Returns:
        Nothing.
    """
    if not self.val:
        self.val = val
        return

    if val < self.val:
        if self.left:
            self.left.insert(val)
        else:
            self.left = TreeNode(val)
    elif val > self.val:
        if self.right:
            self.right.insert(val)
        else:
            self.right = TreeNode(val)
    else:
        return


def getChildren(self, children=[]):  # noqa: 102
    """Gets children of a TreeNode in left-to-right order.

    args:
        children: List of values of children found so far.

    returns:
        List of values of children.
    """
    children.append(self.val)

    if self.left:
        children = self.left.getChildren(children)

    if self.right:
        children = self.right.getChildren(children)

    return children


def findNode(self, val):
    """Searches for a node matching the input.

    args:
        val: Integer to search for.

    returns:
        TreeNode matching the input, or None if no match found.
    """
    if self.val == val:
        return self

    if self.left is None and self.right is None:
        return None

    if val < self.val:
        if self.left is None:
            return None

        return self.left.findNode(val)

    if self.right is None:
        return None

    return self.right.findNode(val)


TreeNode.insert = insert
TreeNode.getChildren = getChildren
TreeNode.findNode = findNode


class TestSolution(unittest.TestCase):
    """Unit tests for Solution."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        solution = Solution()

        root = TreeNode()

        for branch in [4, 2, 7, 1, 3]:
            root.insert(branch)

        val = 2

        result = solution.searchBST(root, val).getChildren()

        self.assertEqual(result, [2, 1, 3], "")

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        solution = Solution()

        val = 5

        root = TreeNode()

        for branch in [4, 2, 7, 1, 3]:
            root.insert(branch)

        result = solution.searchBST(root, val)
        self.assertEqual(result, None, "")


class Solution:
    """Leetcode-provided Solution class."""

    def searchBST(self, root: TreeNode, val: int) -> TreeNode:  # noqa pylint: no-self-use
        """Searches binary tree for a value.

        args:
            root: TreeNode to search.
            val: Integer value to search for.

        Returns:
            TreeNode matching search input and all subsequent children; if no match is
            found, returns None.
        """

        return root.findNode(val)


if __name__ == "__main__":
    unittest.main()
