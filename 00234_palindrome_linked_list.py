"""Leetcode Problem 234
https://leetcode.com/problems/palindrome-linked-list/
Runtime: 892 ms, faster than 76.09% of Python3 online submissions for Palindrome Linked List.
Memory Usage: 46.7 MB, less than 44.53% of Python3 online submissions for Palindrome Linked List.
"""

import unittest


class ListNode:
    """A singly-linked list following the Leetcode implementation."""

    def __init__(self, val=0, next=None):  # noqa pylint: redefined-builtin
        self.val = val
        self.next = next


class TestSolution(unittest.TestCase):
    """Unit tests for Solution. Tests provided examples 1-3."""

    def testExample1(self):
        """Test example 1 provided by Leetcode."""
        head = ListNode(1, ListNode(2, ListNode(2, ListNode(1))))

        solution = Solution()
        is_palindrome = solution.isPalindrome(head)

        self.assertEqual(
            is_palindrome, True, "ListNode([1, 2, 2, 1]) should be a palindrome"
        )

    def testExample2(self):
        """Test example 2 provided by Leetcode."""
        head = ListNode(1, ListNode(2))

        solution = Solution()
        is_palindrome = solution.isPalindrome(head)

        self.assertEqual(
            is_palindrome, False, "ListNode([1, 2]) should not be a palindrome"
        )


class Solution:
    """Leetcode-provided Solution class."""

    def isPalindrome(self, head: ListNode) -> bool:  # noqa pylint: no-self-use
        """Compares the input to a mirrored version and returns True if both are the same,
        False if not.

        Args:
            head: Listnode describing a number.

        Returns:
            True if the input is a palindrome, False if it is not.
        """
        values = []
        curr_node = head

        while True:
            values.append(curr_node.val)

            if not curr_node.next:
                break

            curr_node = curr_node.next

        return values == list(reversed(values))


if __name__ == "__main__":
    unittest.main()
